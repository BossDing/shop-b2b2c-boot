# shop-b2b2c-boot

#### 介绍
基于SpringBoot的多商户的商城平台，接私货利器，有小程序，uni-app版的移动端开发中。


#### 软件架构
SpringBoot+MyBatisPlus-ElasticSearch+Redis+Ecache+MySQL


#### 安装教程

[见Wiki](https://gitee.com/BossDing/shop-b2b2c-boot/wikis **粗体** )

#### 使用说明

[见Wiki](https://gitee.com/BossDing/shop-b2b2c-boot/wikis)
https://gitee.com/BossDing/shop-b2b2c-boot/wikis

#### 参与贡献
添加微信，详聊需求与业务，获取源码


#### 案例
![输入图片说明](https://images.gitee.com/uploads/images/2020/0411/124524_8e14c4d5_1455685.png "截屏2020-04-11 下午12.43.57.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0411/124512_f6ccb4d6_1455685.png "截屏2020-04-11 下午12.44.12.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0411/124533_37d6e3c6_1455685.png "截屏2020-04-11 下午12.44.03.png")

#### 获取方式
| 微信赞助 | 微信 | 支付宝赞助 |
| ---- | -------- | ---------- |
|   | ![微信添加好友](https://images.gitee.com/uploads/images/2020/0411/122259_b62dd404_1455685.jpeg "8934D071D5D72574DD9BB314E061EA0D.jpg")         |          |